package se.socialcooking.test;

import java.util.ArrayList;
import java.util.List;

import se.socialcooking.data.Recipe;
import se.socialcooking.ui.activities.RecipeListActivity;
import se.socialcooking.ui.adapters.RecipeAdapter;
import android.app.Instrumentation;
import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.util.Log;
import android.widget.ListView;

/**
 * Test case for the RecipeListActivity with RecipeAdapter and see that it
 * works as expected.
 * 
 * @author mikaelahlen
 *
 */
public class RecipeListActivityTest extends ActivityUnitTestCase<RecipeListActivity> {
	

	public RecipeListActivityTest() {
		super(RecipeListActivity.class);
	}

	private RecipeAdapter adapter;
	private RecipeListActivity activity;
	private Instrumentation instrumentation;
	
	private ListView listView;
	
	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		super.setUp();
		
		this.instrumentation = getInstrumentation();
		
		startActivity(new Intent(instrumentation.getTargetContext(), RecipeListActivity.class), null, null);
		
		this.activity = getActivity();
		
		List<Recipe> recipes = new ArrayList<Recipe>();
		recipes.add(new Recipe(1, "Recept 1", 4.0));
		recipes.add(new Recipe(2, "Recept 2", 5.0));
		recipes.add(new Recipe(3, "Recept 3", 3.5));
		
		this.adapter = new RecipeAdapter(this.instrumentation.getContext(), android.R.id.list, recipes);
		
		this.activity.setListAdapter(adapter);
		
		this.listView = (ListView)this.activity.getListView();
	}
	
	public void testListViewCount() {
		assertEquals(3, listView.getCount());
	}
	
	public void testNameInList() {
		Recipe recipe = (Recipe)listView.getItemAtPosition(0);
		assertEquals("Recept 1", recipe.title);
	}
}
