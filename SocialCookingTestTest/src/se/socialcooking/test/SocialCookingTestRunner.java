package se.socialcooking.test;

import junit.framework.TestSuite;
import android.test.InstrumentationTestRunner;
import android.test.InstrumentationTestSuite;

public class SocialCookingTestRunner extends InstrumentationTestRunner {
	@Override
	public TestSuite getAllTests() {
		// TODO Auto-generated method stub
		InstrumentationTestSuite suite = new InstrumentationTestSuite(this);
		
		suite.addTestSuite(RecipeAdapterTest.class);
		suite.addTestSuite(RecipeListActivityTest.class);
		
		return suite;
	}
}
