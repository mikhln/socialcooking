package se.socialcooking.test;

import java.util.ArrayList;
import java.util.List;

import android.test.AndroidTestCase;
import se.socialcooking.R;
import se.socialcooking.data.Recipe;
import se.socialcooking.ui.adapters.*;

/**
 * Test case for the RecipeAdapter to see that it actually works
 * 
 * @author mikaelahlen
 *
 */
public class RecipeAdapterTest extends AndroidTestCase {
	private RecipeAdapter adapter;
	
	public RecipeAdapterTest() {
		super();
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		List<Recipe> recipes = new ArrayList<Recipe>();
		recipes.add(new Recipe(1, "Recept 1", 4.0));
		recipes.add(new Recipe(2, "Recept 2", 5.0));
		recipes.add(new Recipe(3, "Recept 3", 3.5));
		
		this.adapter = new RecipeAdapter(getContext(), R.layout.listview_recipe_item, recipes);
	}
	
	public void testCountDataAdapter() {
		assertEquals(3, adapter.getCount());
	}
	
	public void testRecipeId() {
		Recipe recipe = this.adapter.getItem(0);
		assertEquals(1, recipe.id);
	}
	
	public void testRecipeTitle() {
		Recipe recipe = this.adapter.getItem(0);
		assertEquals("Recept 1", recipe.title);
	}
	
	public void testRecipeRating() {
		Recipe recipe = this.adapter.getItem(0);
		assertEquals(4.0, recipe.rating);
	}
	
}
