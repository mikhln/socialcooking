package se.socialcooking.utils;

import java.io.InputStream;
import java.net.URL;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.ImageView;

/**
 * Loads an image asynchronous from the web.
 * 
 * Based of loadImageFromWeb from http://stackoverflow.com/a/6407554
 * 
 * @author mikaelahlen
 *
 */
public class ImageLoaderAsyncTask extends AsyncTask<ImageView, Void, Integer> {

	private Drawable drawable;
	private ImageView imageView;
	private String token;
	
	public ImageLoaderAsyncTask(String token) {
		this.token = token;
	}
	
	private Drawable loadImageFromWeb(String url) {
	    try {
	    	
	        InputStream is = (InputStream) new URL(url).getContent();
	        Drawable d = Drawable.createFromStream(is, null);
	        return d;
	    } catch (Exception e) {
	        return null;
	    }
	}
	
	@Override
	protected Integer doInBackground(ImageView... views) {
		if (views.length < 1) {
			return 0; // no image view to use!
		}
		this.imageView = views[0];
		
		this.drawable = loadImageFromWeb(views[0].getTag().toString() + "?token=" + token);
		//Log.e("TOKEN", authManager.getToken());
		return 0;
	}
	
	//@SuppressLint("NewApi")
	@Override
	protected void onPostExecute(Integer result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		
		this.imageView.setImageDrawable(this.drawable);
	}
}