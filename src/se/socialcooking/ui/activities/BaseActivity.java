package se.socialcooking.ui.activities;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

/**
 * Base activity used by implementations of activities in the application 
 * 
 * @author mikaelahlen
 *
 */
public abstract class BaseActivity extends Activity {
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// set the orientation to portrait only
		setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}
}
