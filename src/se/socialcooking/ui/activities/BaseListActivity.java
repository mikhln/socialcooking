package se.socialcooking.ui.activities;

import android.app.ListActivity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

/**
 * Base list activity used by implementations of list activities in the application 
 * 
 * @author mikaelahlen
 *
 */
public abstract class BaseListActivity extends ListActivity {
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// set the orientation to portrait only
		setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}
}
