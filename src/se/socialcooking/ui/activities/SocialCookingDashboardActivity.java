package se.socialcooking.ui.activities;

import se.socialcooking.R;
import se.socialcooking.R.id;
import se.socialcooking.sessions.AuthManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

/**
 * Activity for the main menu/entry in the application
 * 
 * @author mikaelahlen
 *
 */
public class SocialCookingDashboardActivity extends BaseActivity {

	private AuthManager authManager;
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
		this.authManager = new AuthManager(getApplicationContext());
		this.authManager.checkAuthentication();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_social_cooking_dashboard);
		
		RelativeLayout newRecipe = (RelativeLayout)findViewById(id.dashboard_new_recipe_item);
		if (newRecipe != null) { 
			newRecipe.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Toast t = Toast.makeText(SocialCookingDashboardActivity.this, "Nytt recept �r inte implementerat", Toast.LENGTH_SHORT);
					t.show();
				}
			});
		}
		RelativeLayout searchRecipes = (RelativeLayout)findViewById(id.dashboard_search_item);
		if (searchRecipes != null) { 
			searchRecipes.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Toast t = Toast.makeText(SocialCookingDashboardActivity.this, "S�k recept �r inte implementerat", Toast.LENGTH_SHORT);
					t.show();
				}
			});
		}
		
		RelativeLayout settingsItem = (RelativeLayout)findViewById(id.dashboard_settings_item);
		if (settingsItem != null) { 
			settingsItem.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Toast t = Toast.makeText(SocialCookingDashboardActivity.this, "Inst�llningar �r inte implementerat", Toast.LENGTH_SHORT);
					t.show();
				}
			});
		}
		
		RelativeLayout myRecipes = (RelativeLayout)findViewById(id.dashboard_my_recipes_item);
		if (myRecipes != null) { 
			myRecipes.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// send to the RecipeListActivity (later: with correct params)
					Intent intent = new Intent(SocialCookingDashboardActivity.this, RecipeListActivity.class);
					startActivity(intent);
				}
			});
		}
		
		RelativeLayout myFavvos = (RelativeLayout)findViewById(id.dashboard_favourites_item);
		if (myFavvos != null) { 
			myFavvos.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// send to the RecipeListActivity (later: with correct params)
					Intent intent = new Intent(SocialCookingDashboardActivity.this, RecipeListActivity.class);
					startActivity(intent);
				}
			});
		}
		
		RelativeLayout todos = (RelativeLayout)findViewById(id.dashboard_todo_item);
		if (todos != null) { 
			todos.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// send to the RecipeListActivity (later: with correct params)
					Intent intent = new Intent(SocialCookingDashboardActivity.this, RecipeListActivity.class);
					startActivity(intent);
				}
			});
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.social_cooking_dashboard, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId() == R.id.dashboard_logout) {
			this.authManager.logout();
		}
		
		return super.onOptionsItemSelected(item);
	}

}
