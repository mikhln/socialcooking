package se.socialcooking.ui.activities;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import se.socialcooking.R;
import se.socialcooking.R.layout;
import se.socialcooking.R.menu;
import se.socialcooking.data.Recipe;
import se.socialcooking.data.SocialCookingAPI;
import se.socialcooking.sessions.AuthManager;
import se.socialcooking.ui.adapters.RecipeAdapter;
import se.socialcooking.utils.ConnectionDetector;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.AsyncTaskLoader;
import android.content.Loader;
import android.app.LoaderManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.RelativeLayout;
import android.widget.Toast;

/**
 * Activity for the listing of recipes.
 * 
 * @author mikaelahlen
 *
 */
public class RecipeListActivity extends BaseListActivity implements LoaderManager.LoaderCallbacks<List<Recipe>> {

	RecipeAdapter adapter;
	
	RelativeLayout loadingLayout;
	
	private AuthManager authManager;
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
		this.authManager = new AuthManager(getApplicationContext());
		this.authManager.checkAuthentication();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_recipe_list);

		ListView list = (ListView) findViewById(android.R.id.list);
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent x = new Intent(RecipeListActivity.this, RecipeDetailsActivity.class);
				
				// get the recipe and put the id to the activity
				Recipe recipe = (Recipe)arg0.getItemAtPosition(arg2);
				x.putExtra("recipe_id", recipe.id);
				startActivity(x);
			}

		});
		
		loadingLayout = (RelativeLayout)findViewById(R.id.recipeLoadingLayout);
		
		getListView().setVisibility(View.INVISIBLE);
		
		ConnectionDetector cd = new ConnectionDetector(
				getApplicationContext());
		
		if (!cd.isConnectingToInternet()) {
			Toast.makeText(getApplicationContext(), "Ej ansluten till Internet", Toast.LENGTH_LONG).show();
			loadingLayout.setVisibility(View.INVISIBLE);
			return;
		} else {
			// initalizing the loading of recipes
			getLoaderManager().initLoader(0, null, this).forceLoad();
		}
		
	}
	
	/**
	 * Loader that loads recipes
	 * 
	 * @author mikaelahlen
	 *
	 */
	public static class RecipeListLoader extends AsyncTaskLoader<List<Recipe>> {

		List<Recipe> recipes;
		
		private SocialCookingAPI api;
		
		public RecipeListLoader(Context context) {
			super(context);
			// TODO Auto-generated constructor stub
			api = new SocialCookingAPI(new AuthManager(context).getToken());
		}

		@Override
		public List<Recipe> loadInBackground() {
			Log.e("TEST", "loadInBackground");
			
			List<Recipe> data = new ArrayList<Recipe>();
			
			try {
				data.addAll(this.api.getMyRecipes());
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				Toast.makeText(this.getContext(), "Ett fel intr�ffade", Toast.LENGTH_SHORT).show();
				e.printStackTrace();
			} catch (IOException e) {
				Toast.makeText(this.getContext(), "Ett fel intr�ffade", Toast.LENGTH_SHORT).show();
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				Toast.makeText(this.getContext(), "Ett fel intr�ffade", Toast.LENGTH_SHORT).show();
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return data;
		}
		
	}

	@Override
	public Loader<List<Recipe>> onCreateLoader(int id, Bundle args) {
		// TODO Auto-generated method stub
		Log.i("TEST", "onCreateLoader");
		return new RecipeListLoader(this);
	}

	@Override
	public void onLoadFinished(Loader<List<Recipe>> arg0, List<Recipe> arg1) {
		Log.i("TEST", "onLoadFinished");
		setListAdapter(new RecipeAdapter(this, R.layout.listview_recipe_item, arg1));
		
		loadingLayout.setVisibility(View.INVISIBLE);
		getListView().setVisibility(View.VISIBLE);
	}

	@Override
	public void onLoaderReset(Loader<List<Recipe>> arg0) {
		// TODO Auto-generated method stub
		
		Log.i("TEST", "onLoaderReset");
		setListAdapter(null);
	}
}
