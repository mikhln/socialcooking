package se.socialcooking.ui.activities;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.datatype.Duration;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import se.socialcooking.R;
import se.socialcooking.R.id;
import se.socialcooking.R.layout;
import se.socialcooking.R.menu;
import se.socialcooking.R.string;
import se.socialcooking.data.Recipe;
import se.socialcooking.data.Review;
import se.socialcooking.data.SocialCookingAPI;
import se.socialcooking.sessions.AuthManager;
import se.socialcooking.ui.adapters.RecipeAdapter;
import se.socialcooking.ui.adapters.ReviewAdapter;
import se.socialcooking.utils.ConnectionDetector;
import se.socialcooking.utils.ImageLoaderAsyncTask;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.pm.ActivityInfo;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.NavUtils;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Activity that handles the details view of a recipe with two fragments,
 *  the details with information about the recipe and the other fragment which
 *  list reviews of the recipe.
 *  
 *  Note: based from the ActionBar auto-generation in the Android ADT tool.
 * 
 * @author mikaelahlen
 *
 */
public class RecipeDetailsActivity extends FragmentActivity implements
		ActionBar.TabListener {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	private Integer recipeId;
	private AuthManager authManager;

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

		this.authManager = new AuthManager(getApplicationContext());
		this.authManager.checkAuthentication();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_recipe_details);

		// set the orientation to portrait only
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// For each of the sections in the app, add a tab to the action bar.
		/*
		 * for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) { //
		 * Create a tab with text corresponding to the page title defined by //
		 * the adapter. Also specify this Activity object, which implements //
		 * the TabListener interface, as the callback (listener) for when //
		 * this tab is selected. actionBar.addTab(actionBar.newTab()
		 * .setText(mSectionsPagerAdapter.getPageTitle(i))
		 * .setTabListener(this)); }
		 */
		actionBar.addTab(actionBar.newTab().setText("Recept")
				.setTabListener(this));

		actionBar.addTab(actionBar.newTab().setText("Omd�men")
				.setTabListener(this));

		// read which recipe to handle which is passed through an Intent from
		// other activity.
		this.recipeId = this.getIntent().getIntExtra("recipe_id", -1);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.recipe_details, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == R.id.recipe_write_review) {
			DialogFragment d = new NewReviewDialog();
			d.show(getSupportFragmentManager(), "NewReviewDialog");
		}

		if (item.getItemId() == R.id.recipe_add_as_favourite) {
			DialogFragment d = new FavouriteOrTodoDialog();
			new LoadRecipeArchivesAsyncTask(this.authManager.getToken())
					.execute(d);
			// d.show(getSupportFragmentManager(), "FavouriteOrTodoDialog");
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.

			if (position == 0) {
				Fragment fragment = new RecipeDetailsFragment();
				Bundle args = new Bundle();
				args.putInt(RecipeDetailsFragment.ARG_SECTION_NUMBER,
						position + 1);
				// pass the recipe id to the fragment
				args.putInt("recipe_id", RecipeDetailsActivity.this.recipeId);
				fragment.setArguments(args);
				return fragment;
			}

			// else (since we only have 2 tabs :-) )
			ListFragment fragment = new RecipeReviewsFragment();
			Bundle args = new Bundle();
			args.putInt(RecipeReviewsFragment.ARG_SECTION_NUMBER, position + 1);
			// pass the recipe id to the fragment
			args.putInt("recipe_id", RecipeDetailsActivity.this.recipeId);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			return 2;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			}
			return null;
		}
	}

	/**
	 * A fragment that displays the recipe description with a photo and text.
	 */
	public static class RecipeDetailsFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";

		private AuthManager authManager;

		private TextView recipeAuthorWithDate;
		private TextView recipeTitle;
		private TextView recipeDescription;
		private TextView recipeRating;
		private ImageView recipePhoto;

		private LinearLayout recipeIngredients;

		private RelativeLayout loadingBarLayout;
		private ScrollView recipeDetailsScrollView;

		private Integer recipeId;

		public RecipeDetailsFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(
					R.layout.fragment_recipe_details_view, container, false);

			this.recipeId = getArguments().getInt("recipe_id");

			// set layout and scroll view for the loading bar and details layout
			this.recipeDetailsScrollView = (ScrollView) rootView
					.findViewById(R.id.recipe_details_layout);
			this.loadingBarLayout = (RelativeLayout) rootView
					.findViewById(R.id.recipe_details_loading);

			this.recipeTitle = (TextView) rootView
					.findViewById(R.id.recipe_title);
			this.recipePhoto = (ImageView) rootView
					.findViewById(R.id.recipe_details_image);
			this.recipeAuthorWithDate = (TextView) rootView
					.findViewById(R.id.recipe_author_with_date);
			this.recipeDescription = (TextView) rootView
					.findViewById(R.id.recipe_ingredients_howto);
			this.recipeRating = (TextView) rootView
					.findViewById(R.id.recipe_details_rating);

			this.recipeIngredients = (LinearLayout) rootView
					.findViewById(R.id.recipe_ingredient_contents);

			authManager = new AuthManager(getActivity().getApplicationContext());

			// load the recipe data
			new RecipeDetailsAsyncTask(authManager.getToken())
					.execute(this.recipeId);

			return rootView;
		}

		/**
		 * Async task class that loads the data to the details view.
		 * 
		 * @author mikaelahlen
		 *
		 */
		public class RecipeDetailsAsyncTask extends
				AsyncTask<Integer, Void, Recipe> {
			private SocialCookingAPI api;
			private String token;

			public RecipeDetailsAsyncTask(String token) {
				this.token = token;
				this.api = new SocialCookingAPI(token);
			}

			@Override
			protected Recipe doInBackground(Integer... params) {
				// TODO Auto-generated method stub
				ConnectionDetector cd = new ConnectionDetector(
						RecipeDetailsFragment.this.getActivity()
								.getApplicationContext());

				if (cd.isConnectingToInternet()) {

					if (params.length > 0) {
						try {
							return api.getRecipe(params[0]);
						} catch (ClientProtocolException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				return null;
			}

			@Override
			protected void onPostExecute(Recipe recipe) {
				// TODO Auto-generated method stub
				super.onPostExecute(recipe);

				if (recipe != null) {
					// set the recipe data to the view
					RecipeDetailsFragment.this.recipeDetailsScrollView
							.setVisibility(View.VISIBLE);
					RecipeDetailsFragment.this.loadingBarLayout
							.setVisibility(View.INVISIBLE);

					RecipeDetailsFragment.this.recipeTitle
							.setText(recipe.title);

					Date recipeDate = new Date(recipe.createdAt);
					SimpleDateFormat formattedDate = new SimpleDateFormat(
							"yyyy-MM-dd");
					RecipeDetailsFragment.this.recipeAuthorWithDate
							.setText("Av " + recipe.author + ", "
									+ formattedDate.format(recipeDate));

					RecipeDetailsFragment.this.recipeDescription
							.setText(recipe.description);

					RecipeDetailsFragment.this.recipeRating.setText(new String(
							new char[(int) recipe.rating]).replace("\0",
							"\u2605"));

					for (Recipe.Ingredient ingredient : recipe.ingredients) {
						TextView ingredientTextView = new TextView(
								RecipeDetailsFragment.this.recipeIngredients
										.getContext());
						ingredientTextView.setLayoutParams(new LayoutParams(
								LinearLayout.LayoutParams.WRAP_CONTENT,
								LinearLayout.LayoutParams.WRAP_CONTENT));
						ingredientTextView.setText(ingredient.toString());
						RecipeDetailsFragment.this.recipeIngredients
								.addView(ingredientTextView);
					}

					RecipeDetailsFragment.this.recipePhoto
							.setTag(recipe.photoUrl);
					new ImageLoaderAsyncTask(this.token)
							.execute(RecipeDetailsFragment.this.recipePhoto);
				} else {
					ConnectionDetector cd = new ConnectionDetector(
							RecipeDetailsFragment.this.getActivity()
									.getApplicationContext());

					if (!cd.isConnectingToInternet()) {
						Toast.makeText(
								RecipeDetailsFragment.this.getActivity()
										.getApplicationContext(),
								"Ej ansluten till Internet", Toast.LENGTH_LONG)
								.show();
					} else {
						Toast.makeText(
								RecipeDetailsFragment.this.getActivity()
										.getApplicationContext(),
								"Kunde inte ladda in receptet",
								Toast.LENGTH_SHORT).show();
					}
					getActivity().finish();
				}
			}

		}
	}

	/**
	 * Fragment that list reviews of the review.
	 * 
	 * Note: only a prototype which means that the data is static.
	 * 
	 * @author mikaelahlen
	 *
	 */
	public static class RecipeReviewsFragment extends ListFragment implements
			LoaderManager.LoaderCallbacks<List<Review>> {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";

		public RecipeReviewsFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(
					R.layout.fragment_recipe_details_reviews, container, false);

			List<Review> data = new ArrayList<Review>();
			data.add(new Review(
					1,
					"Mikael",
					"Detta �r riktigt gott! Har tillagat detta flera g�nger med lyckade resultat vid varje tillf�lle!",
					5));
			data.add(new Review(
					1,
					"Kocken",
					"Smakar riktigt bra och v�ldigt l�tt att tillaga, �ven f�r mig som �r v�rldens b�sta kock. "
							+ "Jag skulle �ven vilja tipsa om att blanda in lite f�rska b�r, l�mpligtvis hallon och bl�b�r f�r att f� en extra touch!",
					5));
			data.add(new Review(
					1,
					"Smakl�ken",
					"Detta �r riktigt gott! Har tillagat detta flera g�nger med lyckade resultat vid varje tillf�lle!",
					5));
			data.add(new Review(
					1,
					"Gourmemannen",
					"Detta �r riktigt gott! Har tillagat detta flera g�nger med lyckade resultat vid varje tillf�lle!",
					5));
			data.add(new Review(
					1,
					"Pankaksmannen",
					"Detta �r riktigt gott! Har tillagat detta flera g�nger med lyckade resultat vid varje tillf�lle!",
					5));
			data.add(new Review(
					1,
					"Konditorn",
					"Detta �r riktigt gott! Har tillagat detta flera g�nger med lyckade resultat vid varje tillf�lle!",
					5));
			data.add(new Review(
					1,
					"Mikael",
					"Detta �r riktigt gott! Har tillagat detta flera g�nger med lyckade resultat vid varje tillf�lle!",
					5));
			data.add(new Review(
					1,
					"Mikael",
					"Detta �r riktigt gott! Har tillagat detta flera g�nger med lyckade resultat vid varje tillf�lle!",
					5));
			data.add(new Review(
					1,
					"Mikael",
					"Detta �r riktigt gott! Har tillagat detta flera g�nger med lyckade resultat vid varje tillf�lle!",
					5));
			data.add(new Review(
					1,
					"Mikael",
					"Detta �r riktigt gott! Har tillagat detta flera g�nger med lyckade resultat vid varje tillf�lle!",
					5));
			data.add(new Review(
					1,
					"Mikael",
					"Detta �r riktigt gott! Har tillagat detta flera g�nger med lyckade resultat vid varje tillf�lle!",
					5));
			data.add(new Review(
					1,
					"Mikael",
					"Detta �r riktigt gott! Har tillagat detta flera g�nger med lyckade resultat vid varje tillf�lle!",
					5));
			data.add(new Review(
					1,
					"Mikael",
					"Detta �r riktigt gott! Har tillagat detta flera g�nger med lyckade resultat vid varje tillf�lle!",
					5));
			data.add(new Review(
					1,
					"Mikael",
					"Detta �r riktigt gott! Har tillagat detta flera g�nger med lyckade resultat vid varje tillf�lle!",
					5));

			ReviewAdapter adapter = new ReviewAdapter(getActivity(),
					R.layout.listview_review_item, data);

			this.setListAdapter(adapter);

			Button newReviewBtn = (Button) rootView
					.findViewById(R.id.reviewNewButton);
			newReviewBtn.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					DialogFragment d = new NewReviewDialog();
					d.show(getActivity().getSupportFragmentManager(),
							"NewReviewDialog");
				}
			});

			return rootView;
		}

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onActivityCreated(savedInstanceState);
		}

		@Override
		public Loader<List<Review>> onCreateLoader(int arg0, Bundle arg1) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void onLoadFinished(Loader<List<Review>> arg0, List<Review> arg1) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onLoaderReset(Loader<List<Review>> arg0) {
			// TODO Auto-generated method stub

		}
	}

	/**
	 * Async task that loads which archives the user have added the recipe in.
	 * 
	 * @author mikaelahlen
	 *
	 */
	public class LoadRecipeArchivesAsyncTask extends
			AsyncTask<DialogFragment, Void, boolean[]> {

		private DialogFragment dialog;

		private SocialCookingAPI api;

		public LoadRecipeArchivesAsyncTask(String token) {
			this.api = new SocialCookingAPI(token);
		}

		@Override
		protected boolean[] doInBackground(DialogFragment... params) {
			if (params.length < 1)
				return null;
			this.dialog = params[0];

			ConnectionDetector cd = new ConnectionDetector(
					RecipeDetailsActivity.this.getApplicationContext());

			if (cd.isConnectingToInternet()) {

				try {
					return api.getRecipeArchives(recipeId);
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					Log.e("LoadRecipeArchivesAsyncTask", "ERROR");
					e.printStackTrace();
				} catch (IOException e) {
					Log.e("LoadRecipeArchivesAsyncTask", "ERROR");
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					Log.e("LoadRecipeArchivesAsyncTask", "ERROR");
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return new boolean[] { false, false };
			// return null; // or { false, false } ?
		}

		@Override
		protected void onPostExecute(boolean[] result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			ConnectionDetector cd = new ConnectionDetector(
					RecipeDetailsActivity.this.getApplicationContext());

			if (!cd.isConnectingToInternet()) {
				Toast.makeText(getApplicationContext(),
						"Ej ansluten till Internet", Toast.LENGTH_LONG).show();
				return;
			}

			Bundle bundle = new Bundle();
			bundle.putBooleanArray("selectedItems", result);
			bundle.putInt("recipeId", recipeId);
			dialog.setArguments(bundle);
			dialog.show(getSupportFragmentManager(), "FavouriteOrTodoDialog");
		}

	}

	/**
	 * Dialog that handles the UI for adding or removing which archives the recipe
	 * should be in.
	 * 
	 * @author mikaelahlen
	 *
	 */
	public static class FavouriteOrTodoDialog extends DialogFragment {
		private ArrayList<Integer> selectedItems;
		private boolean[] archivedItems;
		private Integer recipeId;

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			this.archivedItems = this.getArguments().getBooleanArray(
					"selectedItems");
			this.recipeId = this.getArguments().getInt("recipeId");

			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

			selectedItems = new ArrayList<Integer>();
			if (archivedItems[0] == true) {
				this.selectedItems.add(0);
			}
			if (archivedItems[1] == true) {
				this.selectedItems.add(1);
			}

			if (archivedItems != null)
				Log.e("SELECTED-WEB", String.valueOf(archivedItems.length));
			else
				Log.e("SELECTED-WEB", "FEL!");

			builder.setTitle("V�lj arkiv").setMultiChoiceItems(
					new String[] { "Favoriter", "Att g�ra" },
					this.archivedItems, new OnMultiChoiceClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which,
								boolean isChecked) {
							if (isChecked) {
								selectedItems.add(which);
							} else if (selectedItems.contains(which)) {
								selectedItems.remove((Object) which);
							}
						}

					});

			builder.setPositiveButton("Spara", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					Log.e("SELECTED", selectedItems.toString());

					AuthManager authManager = new AuthManager(getActivity()
							.getApplicationContext());

					ConnectionDetector cd = new ConnectionDetector(
							getActivity().getApplicationContext());

					if (!cd.isConnectingToInternet()) {
						Toast.makeText(getActivity().getApplicationContext(),
								"Ej ansluten till Internet", Toast.LENGTH_LONG)
								.show();
						return;
					}

					// submit the new choices
					new SaveArchivesAsyncTask(recipeId, authManager.getToken(),
							selectedItems).execute();
				}
			});

			builder.setNegativeButton("Avbryt", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub

				}
			});

			/*
			 * .setItems(new String[] {"Favoriter", "Att g�ra"}, new
			 * DialogInterface.OnClickListener() { public void
			 * onClick(DialogInterface dialog, int which) { // The 'which'
			 * argument contains the index position // of the selected item }
			 */
			return builder.create();
		}

		/**
		 * Async task that handles the saving of the archive for the recipe by the user.
		 * 
		 * @author mikaelahlen
		 *
		 */
		public class SaveArchivesAsyncTask extends AsyncTask<Void, Void, Void> {

			private List<Integer> selectedItems;
			private String token;
			private Integer recipeId;

			public SaveArchivesAsyncTask(Integer recipeId, String token,
					List<Integer> selectedItems) {
				this.recipeId = recipeId;
				this.selectedItems = selectedItems;
				this.token = token;
			}

			@Override
			protected Void doInBackground(Void... params) {
				// TODO Auto-generated method stub

				SocialCookingAPI api = new SocialCookingAPI(token);

				ConnectionDetector cd = new ConnectionDetector(
						getActivity().getApplicationContext());

				if (cd.isConnectingToInternet()) {

					try {
						api.saveRecipeArchives(recipeId,
								selectedItems.contains(0),
								selectedItems.contains(1));
					} catch (ClientProtocolException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				return null;
			}

		}

	}

	/**
	 * Dialog for a review but is only a prototype without functionality but UI.
	 * 
	 * @author mikaelahlen
	 *
	 */
	public static class NewReviewDialog extends DialogFragment {
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			LayoutInflater inflater = getActivity().getLayoutInflater();

			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setView(inflater.inflate(R.layout.dialog_new_review, null));

			builder.setTitle("Skriv omd�me");

			builder.setPositiveButton("Spara", new OnClickListener() {

				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					// TODO Auto-generated method stub
					Toast.makeText(getActivity().getApplicationContext(),
							"Omd�men �r inte implementerat", Toast.LENGTH_SHORT)
							.show();
				}

			});

			builder.setNegativeButton("Avbryt", new OnClickListener() {

				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					// TODO Auto-generated method stub
					Toast.makeText(getActivity().getApplicationContext(),
							"Omd�men �r inte implementerat", Toast.LENGTH_SHORT)
							.show();
				}

			});

			return builder.create();
		}
	}

}
