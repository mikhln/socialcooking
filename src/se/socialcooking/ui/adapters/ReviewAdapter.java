package se.socialcooking.ui.adapters;

import java.util.List;

import se.socialcooking.R;
import se.socialcooking.data.Recipe;
import se.socialcooking.data.Review;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

// Reference: http://www.ezzylearning.com/tutorial.aspx?tid=1763429

/**
 * Adapter that handles the data and view binding for a review.
 * @author mikaelahlen
 *
 */
public class ReviewAdapter extends ArrayAdapter<Review> {

	Context ctx;
	int layoutResourceId;
	List<Review> data = null;
	
	public ReviewAdapter(Context context, int textViewResourceId,
			List<Review> objects) {
		super(context, textViewResourceId, objects);
		
		this.ctx = context;
		this.layoutResourceId = textViewResourceId;
		this.data = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View row = convertView;
		ReviewHolder holder = null;
		
		if (row == null) {
			LayoutInflater inflater = ((Activity)this.ctx).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);
			
			holder = new ReviewHolder();
			holder.reviewName = (TextView)row.findViewById(R.id.reviewName);
			holder.reviewContent = (TextView)row.findViewById(R.id.reviewContent);
			holder.reviewRating = (TextView)row.findViewById(R.id.reviewRating);

			row.setTag(holder);
		} else {
			holder = (ReviewHolder)row.getTag();
		}
		
		Review review= data.get(position);
		holder.reviewName.setText("Skrivet av " + review.author);
		holder.reviewContent.setText(review.comment);
		holder.reviewRating.setText("★★★★★");
		
		return row;
	}
	
	static class ReviewHolder {
		TextView reviewName;
		TextView reviewContent;
		TextView reviewRating;
	}
	
}
