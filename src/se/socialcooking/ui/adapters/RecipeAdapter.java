package se.socialcooking.ui.adapters;

import java.io.InputStream;
import java.net.URL;
import java.util.List;

import se.socialcooking.R;
import se.socialcooking.data.Recipe;
import se.socialcooking.sessions.AuthManager;
import se.socialcooking.utils.ImageLoaderAsyncTask;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

// Reference: http://www.ezzylearning.com/tutorial.aspx?tid=1763429

/**
 * Adapter that handles the data and view binding for a recipe. 
 * @author mikaelahlen
 *
 */
public class RecipeAdapter extends ArrayAdapter<Recipe> {

	Context ctx;
	int layoutResourceId;
	List<Recipe> data = null;
	private AuthManager authManager;
	
	public RecipeAdapter(Context context, int textViewResourceId,
			List<Recipe> objects) {
		super(context, textViewResourceId, objects);
		
		this.ctx = context;
		this.layoutResourceId = textViewResourceId;
		this.data = objects;
		
		this.authManager = new AuthManager(context);
	}
	
	public RecipeAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
		
		this.ctx = context;
		this.layoutResourceId = textViewResourceId;
		
		this.authManager = new AuthManager(context);
	}
	
	public void setData(List<Recipe> data) {
		clear();
		//this.data = data;
		
		if (data == null) {
			addAll(data);
		}
		// or loop for each?
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View row = convertView;
		RecipeHolder holder = null;
		
		if (row == null) {
			LayoutInflater inflater = ((Activity)this.ctx).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);
			
			holder = new RecipeHolder();
			holder.recipeTitle = (TextView)row.findViewById(R.id.recipeTitle);
			holder.recipeImage = (ImageView)row.findViewById(R.id.recipe_image);
			holder.recipeRating = (TextView)row.findViewById(R.id.recipeRating);

			row.setTag(holder);
		} else {
			holder = (RecipeHolder)row.getTag();
		}
		
		Recipe recipe = data.get(position);
		holder.recipeTitle.setText(recipe.title);
		
		// from: http://stackoverflow.com/a/4903603
		holder.recipeRating.setText(new String(new char[(int)recipe.rating]).replace("\0", "\u2605"));
		
		holder.recipeImage.setTag("http://matlagning-hgo.appspot.com/api/recipes/" + String.valueOf(recipe.id) + "/image/thumbnail/");
		new ImageLoaderAsyncTask(authManager.getToken()).execute(holder.recipeImage);
		
		AuthManager authManager = new AuthManager(this.ctx);
		Log.i("TOKEN", authManager.getToken());
		
		return row;
	}
	
	static class RecipeHolder {
		TextView recipeTitle;
		ImageView recipeImage;
		TextView recipeRating;
	}
}
