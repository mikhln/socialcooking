package se.socialcooking.data;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/**
 * API class for the native API wrapper for the RESTful API. All calls needs to
 * be made inside a thread like an AsyncTask.
 * 
 */
public class SocialCookingAPI {
	private String token;

	/**
	 * Constructor for the native API wrapper for the RESTful API.
	 * 
	 * @param apiToken	the API token to use when making request to the RESTful API.
	 */
	public SocialCookingAPI(String apiToken) {
		this.token = apiToken;
	}

	/**
	 * Used to return a list of reviews that is written by the logged in user.
	 * 
	 * @return list of recipes
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 * @throws JSONException 
	 */
	public List<Recipe> getMyRecipes() throws ClientProtocolException, IOException, JSONException {
		HttpClient httpClient = new DefaultHttpClient();

		List<Recipe> recipes = new ArrayList<Recipe>();

		HttpResponse response = httpClient.execute(new HttpGet(
				"http://matlagning-hgo.appspot.com/api/me/recipes/?token="
						+ token));
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		response.getEntity().writeTo(out);

		Log.e("getMyRecipes", out.toString());
		
		if (response.getStatusLine().getStatusCode() == 200) {
			JSONArray jsonRecipes = new JSONArray(out.toString());

			for (int i = 0; i < jsonRecipes.length(); i++) {
				JSONObject r = jsonRecipes.getJSONObject(i);
				Recipe recipe = new Recipe();
				recipe.id = r.getInt("id");
				recipe.title = r.getString("title");
				recipe.rating = r.getDouble("rating");
				recipe.photoUrl = r.getString("photo_url");
				recipes.add(recipe);
			}
		}

		return recipes;
	}
	
	/**
	 * Returns the username of the user associated with the specified user token.
	 * 
	 * @return username of the user with the token
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public String getUsername() throws ClientProtocolException, IOException {
		HttpClient httpClient = new DefaultHttpClient();

		HttpResponse response = httpClient.execute(new HttpGet(
				"http://matlagning-hgo.appspot.com/api/me/?token="
						+ token));
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		response.getEntity().writeTo(out);

		Log.e("getMyRecipes", out.toString());
		
		if (response.getStatusLine().getStatusCode() == 200) {
			return out.toString();
		} else {
			return null;
		}
	}

	/**
	 * Returns a recipe by the specified id from the API. 
	 * 
	 * @param id id of the recipe
	 * @return recipe object of the asked id or null if not found.
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws JSONException
	 * 
	 * @see Recipe
	 */
	public Recipe getRecipe(Integer id) throws ClientProtocolException,
			IOException, JSONException {
		HttpClient httpClient = new DefaultHttpClient();

		HttpResponse response = httpClient.execute(new HttpGet(
				"http://matlagning-hgo.appspot.com/api/recipes/" + id.toString() + "/?token="
						+ token));
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		response.getEntity().writeTo(out);

		if (response.getStatusLine().getStatusCode() == 200) {
			JSONObject jsonRecipe = new JSONObject(out.toString());

			SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			
			Recipe recipe = new Recipe();
			recipe.author = jsonRecipe.getString("author");
			recipe.title = jsonRecipe.getString("title");
			recipe.description = jsonRecipe.getString("body");
			recipe.id = jsonRecipe.getInt("id");
			recipe.photoUrl = jsonRecipe.getString("photo_url") + "header/";
			recipe.rating = jsonRecipe.getDouble("rating");
			try {
				recipe.createdAt = dateParser.parse(jsonRecipe.getString("created_at")).getTime();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				recipe.createdAt = 0L;
			}
			
			JSONArray jsonIngredients = jsonRecipe.getJSONArray("ingredients");
			
			for (int i = 0; i < jsonIngredients.length(); i++) {
				JSONObject jsonIngredient = jsonIngredients.getJSONObject(i);
				recipe.addIngredient(
						jsonIngredient.getInt("id"), 
						jsonIngredient.getString("value"),
						jsonIngredient.getString("unit"),
						jsonIngredient.getDouble("amount"));
			}
			
			return recipe;
		}

		return null;
	}
	
	/**
	 * Returns a list of boolean with length of 2 where each value in the array
	 * is the condition if the user has added the recipe as first, as a favorite, and
	 * the second element if it is added in the todo list, called "todos". 
	 * 
	 * @param id	id of the recipe
	 * @return	an array of two booleans where the first value is if the recipe is added to the favorite list and the second for the todo list.
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws JSONException
	 */
	public boolean[] getRecipeArchives(Integer id) throws ClientProtocolException, IOException, JSONException {
		HttpClient httpClient = new DefaultHttpClient();

		HttpResponse response = httpClient.execute(new HttpGet(
				"http://matlagning-hgo.appspot.com/api/recipes/" + id.toString() + "/archives/?token="
						+ token));
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		response.getEntity().writeTo(out);

		if (response.getStatusLine().getStatusCode() == 200) {
			boolean[] archives = new boolean[2];
			
			JSONObject jsonArchive = new JSONObject(out.toString());
			Boolean favourites = jsonArchive.getBoolean("favourites");
			Boolean todos = jsonArchive.getBoolean("todos");
			
			archives[0] = (favourites == true);
			archives[1] = (todos == true);
			
			return archives;
		}
		
		return null;
	}
	
	/**
	 * Try to save the recipe to the users archive.
	 * 
	 * @param id	id of the recipe
	 * @param favourites	whether the recipe should be added to the users favourite list
	 * @param todos			whether the recipe should be added to the users todo list.
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public void saveRecipeArchives(Integer id, Boolean favourites, Boolean todos) throws ClientProtocolException, IOException {
		Log.e("saveRecipeArchives", favourites.toString() + ", " + todos.toString());
		
		HttpClient httpClient = new DefaultHttpClient();

		HttpPost post = new HttpPost(
				"http://matlagning-hgo.appspot.com/api/recipes/" + id.toString() + "/archives/?token="
						+ token);
		List<NameValuePair> formData = new ArrayList<NameValuePair>();
		formData.add(new BasicNameValuePair("favourites", favourites.toString()));
		formData.add(new BasicNameValuePair("todos", todos.toString()));
		post.setEntity(new UrlEncodedFormEntity(formData));
		
		HttpResponse response = httpClient.execute(post);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		response.getEntity().writeTo(out);

		if (response.getStatusLine().getStatusCode() == 200) {
			Log.e("saveRecipeArchives", out.toString());
		} else {
			Log.e("saveRecipeArchives", out.toString());
			Log.e("saveRecipeArchives-2", post.getURI().toString());
		}
	}
}
