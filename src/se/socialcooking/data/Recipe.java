package se.socialcooking.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Model to hold the data for a recipe
 * 
 * @author mikaelahlen
 *
 */
public class Recipe {
	public int id;
	public String author;
	public String title;
	public double rating;
	public List<Ingredient> ingredients;
	public String description;
	public Long createdAt;
	public String photoUrl;
	
	public Recipe() {
		this.ingredients = new ArrayList<Recipe.Ingredient>();
	}
	
	public Recipe(int id, String title, double rating) {
		this.id = id;
		this.title = title;
		this.rating = rating;
		this.ingredients = new ArrayList<Recipe.Ingredient>();
		this.description = "";
		this.photoUrl = "";
		this.author = "";
	}
	
	/**
	 * Add a new ingredient to the recipe.
	 * 
	 * @param id	id of the ingredient 
	 * @param name	name of the ingredient
	 * @param unit	unit of the ingredient
	 * @param amount	amout of the ingredient
	 */
	public void addIngredient(int id, String name, String unit, double amount) {
		this.ingredients.add(new Ingredient(id, name, unit, amount));
	}
	
	/**
	 * Model for the ingredient
	 * 
	 * @author mikaelahlen
	 *
	 */
	public class Ingredient {
		public int id;
		public String name;
		public String unit;
		public double amount;
		
		public Ingredient(int id, String name, String unit, double amount) {
			this.id = id;
			this.name = name;
			this.unit = unit;
			this.amount = amount;
		}
		
		@Override
		public String toString() {
			return this.amount + " " + this.unit + " " + this.name;
		}
	}
}
