package se.socialcooking.data;

/**
 * Model for the review
 * 
 * @author mikaelahlen
 *
 */
public class Review {
	public int id;
	public String author;
	public String comment;
	public int rating;
	
	public Review(int id, String author, String comment, int rating) {
		this.id = id;
		this.author = author;
		this.comment = comment;
		this.rating = rating;
	}
}