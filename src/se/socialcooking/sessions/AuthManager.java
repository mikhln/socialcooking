package se.socialcooking.sessions;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import se.socialcooking.data.SocialCookingAPI;
import se.socialcooking.ui.activities.SocialCookingLoginActivity;
import se.socialcooking.utils.ConnectionDetector;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

// based on http://www.androidhive.info/2012/08/android-session-management-using-shared-preferences/

/**
 * AuthManager handles the internal authentication in the application
 * 
 * @author mikaelahlen
 *
 */
public class AuthManager {
	private SharedPreferences preferences;
	private Editor editor;
	private Context context;

	public AuthManager(Context ctx) {
		this.context = ctx;
		this.preferences = ctx.getSharedPreferences("SocialCookingUserSession",
				0);
		this.editor = this.preferences.edit();
	}

	/**
	 * sets the user authenticated in the application 
	 * 
	 * @param token	token for the user used for API requests
	 */
	public void authenticate(String token) {
		editor.putBoolean("isLoggedIn", true);
		editor.putString("token", token);

		editor.commit(); // save
	}

	/**
	 * Returns the token for the authenticated user
	 * 
	 * @return token for the authenticated user
	 */
	public String getToken() {
		return this.preferences.getString("token", "");
	}

	/**
	 * Perform a logout from the application and send the user to the login activity.
	 */
	public void logout() {
		this.editor.clear();
		this.editor.commit();

		Intent loginActivity = new Intent(this.context,
				SocialCookingLoginActivity.class);
		// loginActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		loginActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_NEW_TASK);

		this.context.startActivity(loginActivity);
	}

	/**
	 * Checks whether the user is authenticated or not.
	 */
	public void checkAuthentication() {
		if (!this.preferences.getBoolean("isLoggedIn", false)) {
			Intent loginActivity = new Intent(this.context,
					SocialCookingLoginActivity.class);
			loginActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			this.context.startActivity(loginActivity);
		} else {
			new AsyncTask<Void, Void, Boolean>() {

				@Override
				protected Boolean doInBackground(Void... params) {
					// TODO Auto-generated method stub
					SocialCookingAPI api = new SocialCookingAPI(getToken());

					ConnectionDetector cd = new ConnectionDetector(context);

					Boolean isAuthenticated = false;

					if (cd.isConnectingToInternet()) {
						try {
							if (api.getUsername() != null) {
								isAuthenticated = true;
							}
						} catch (ClientProtocolException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							isAuthenticated = false;
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							isAuthenticated = false;
						}
					}

					return isAuthenticated;
				}

				@Override
				protected void onPostExecute(Boolean result) {
					super.onPostExecute(result);

					ConnectionDetector cd = new ConnectionDetector(context);
					
					if (!cd.isConnectingToInternet()) {
						Toast.makeText(context, "Ej ansluten till Internet", Toast.LENGTH_LONG).show();
						return;
					}
					
					if (result == false) {
						Intent loginActivity = new Intent(context,
								SocialCookingLoginActivity.class);
						loginActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_NEW_TASK);
						context.startActivity(loginActivity);
					}
				}
			}.execute();
		}
	}
}
